package kotlin

import (
	"bytes"
	"fmt"
	"strings"
	"text/template"

	"github.com/sirupsen/logrus"
	"gitlab.com/fighters-solution/dynamo/lib"
	"gitlab.com/fighters-solution/dynamo/lib/model"
	"gitlab.com/fighters-solution/dynamo/lib/util"
)

var (
	tmpl   *template.Template
	tcTmpl *template.Template
)

type gen struct{}

func init() {
	var err error
	tmpl, err = template.New("template").Parse(codeTmpl)
	if err != nil {
		logrus.Panicf("python: failed to parse template: %s", err)
	}
	tcTmpl, err = template.New("testCaseTemplate").Parse(testTmpl)
	if err != nil {
		logrus.Panicf("python: failed to parse test case template: %s", err)
	}
	g := &gen{}
	lib.Register(lib.Python3, g)
}

func (g *gen) Generate(in model.Input) (*model.GeneratedTest, error) {
	// Template
	gen := &model.GeneratedTest{
		Language: lib.Python3,
	}

	pArgs, err := parseParams(in.Function.Params)
	if err != nil {
		return nil, err
	}
	pRets, err := parseParams(in.Function.Returns)
	if err != nil {
		return nil, err
	}
	normalizeTestCases(&in, pArgs, pRets)

	tmpl, err := genTemplate(&in.Function, pArgs, pRets)
	if err != nil {
		return nil, err
	}
	gen.Template = tmpl
	// Fixture
	fixture, err := genTestCase(&in, false)
	if err != nil {
		return nil, err
	}
	gen.Fixture = fixture
	// Test case
	tc, err := genTestCase(&in, true)
	if err != nil {
		return nil, err
	}
	gen.TestCase = tc
	return gen, nil
}

func genTemplate(f *model.Function, pArgs []model.Param, pRets []model.Param) (string, error) {
	iFunc, err := parseFunction(f, pArgs, pRets)
	if err != nil {
		return "", err
	}
	b := &bytes.Buffer{}
	tmpl.Execute(b, iFunc)
	return b.String(), nil
}

func parseFunction(f *model.Function, pArgs []model.Param, pRets []model.Param) (*model.InternalFunc, error) {
	iFunc := &model.InternalFunc{
		Name: util.ToSnakeCase(f.Name),
	}
	params := ""
	for _, p := range pArgs {
		params += fmt.Sprintf("%s, ", p.Name)
	}
	params = strings.TrimSuffix(params, ", ")
	iFunc.Param = strings.TrimSuffix(params, ", ")
	iFunc.Return = pRets[0].Name
	return iFunc, nil
}

func genTestCase(in *model.Input, isTestCase bool) (string, error) {
	iTc := &model.InternalTestCase{}
	desc := in.Fixtures.Describe
	if isTestCase {
		desc = in.TestCases.Describe
	}
	if desc == "" {
		desc = util.ToSnakeCase(in.Function.Name)
	}
	iTc.Describe = desc

	iTc.Cases = "start_time = time.time()\n\n"
	fName := util.ToSnakeCase(in.Function.Name)
	for i, c := range in.Fixtures.Cases {
		tc, err := generateCase(c, fName, i)
		if err != nil {
			return "", err
		}
		iTc.Cases += tc + "\n"
	}
	if isTestCase {
		for i, c := range in.TestCases.Cases {
			tc, err := generateCase(c, fName, i+len(in.Fixtures.Cases))
			if err != nil {
				return "", err
			}
			iTc.Cases += tc + "\n"
		}
	}
	iTc.Cases = strings.TrimSuffix(iTc.Cases, "\n\n")
	iTc.Cases += "\nprint(\"<EXECTIME::>{}\".format(time.time() - start_time))"

	b := &bytes.Buffer{}
	tcTmpl.Execute(b, iTc)
	return b.String(), nil
}

func parseParams(params []string) ([]model.Param, error) {
	ret := make([]model.Param, 0)

	for i, p := range params {
		spl := strings.Split(p, " ") // a int
		pName, pType := "", ""
		switch {
		case len(spl) == 1:
			pName = fmt.Sprintf("p%d", i)
			pType = spl[0]
		case len(spl) == 2:
			pName = spl[0]
			pType = spl[1]
		default:
			return nil, fmt.Errorf("python: invalid function parameter: %s", p)
		}

		pArgs := model.Param{
			Name: pName,
		}
		getActualType(&pArgs, pType)
		ret = append(ret, pArgs)
	}
	return ret, nil
}

func getActualType(p *model.Param, t string) {
	if strings.HasSuffix(t, "[]") {
		p.IsArray = true
	}
	if strings.Contains(t, "boolean") {
		p.IsBool = true
	}
}

func generateCase(c model.Case, fName string, idx int) (string, error) {
	ret := fmt.Sprintf("# Test case #%d\n", idx)
	if c.It == "" {
		c.It = fmt.Sprintf("Test case #%d", idx)
	}
	ret += fmt.Sprintf("Test.it(\"%s\")\n", c.It)
	for i, a := range c.Args {
		iName := fmt.Sprintf("%d%d", idx, i)
		ret += fmt.Sprintf("in%s = %s\n", iName, a)
	}
	ret += fmt.Sprintf("ret%d = %s(", idx, fName)
	for i := range c.Args {
		ret += fmt.Sprintf("in%d%d, ", idx, i)
	}
	ret = strings.TrimSuffix(ret, ", ") + ")\n"
	ret += fmt.Sprintf("ex%d = %s\n", idx, c.Expected[0])
	ret += fmt.Sprintf("Test.expect(ret%d == ex%d)\n", idx, idx)
	return ret, nil
}

func normalizeTestCases(in *model.Input, pArgs []model.Param, pRets []model.Param) {
	for i, a := range pArgs {
		if a.IsBool {
			for j := range in.Fixtures.Cases {
				in.Fixtures.Cases[j].Args[i] = strings.Replace(in.Fixtures.Cases[j].Args[i], "false", "False", -1)
				in.Fixtures.Cases[j].Args[i] = strings.Replace(in.Fixtures.Cases[j].Args[i], "true", "True", -1)
			}
			for j := range in.TestCases.Cases {
				in.TestCases.Cases[j].Args[i] = strings.Replace(in.TestCases.Cases[j].Args[i], "false", "False", -1)
				in.TestCases.Cases[j].Args[i] = strings.Replace(in.TestCases.Cases[j].Args[i], "true", "True", -1)
			}
		}
	}

	for i, a := range pRets {
		if a.IsBool {
			for j := range in.Fixtures.Cases {
				in.Fixtures.Cases[j].Expected[i] = strings.Replace(in.Fixtures.Cases[j].Expected[i], "false", "False", -1)
				in.Fixtures.Cases[j].Expected[i] = strings.Replace(in.Fixtures.Cases[j].Expected[i], "true", "True", -1)
			}
			for j := range in.TestCases.Cases {
				in.TestCases.Cases[j].Expected[i] = strings.Replace(in.TestCases.Cases[j].Expected[i], "false", "False", -1)
				in.TestCases.Cases[j].Expected[i] = strings.Replace(in.TestCases.Cases[j].Expected[i], "true", "True", -1)
			}
		}
	}
}

// Templates
var (
	codeTmpl = `def {{.Name}}({{.Param}}):
  # your code here
  `
	testTmpl = `import time

Test.describe("{{.Describe}}")
{{.Cases}}
`
)
