package golang

import (
	"bytes"
	"fmt"
	"strings"
	"text/template"

	"github.com/sirupsen/logrus"
	"gitlab.com/fighters-solution/dynamo/lib"
	"gitlab.com/fighters-solution/dynamo/lib/model"
	"gitlab.com/fighters-solution/dynamo/lib/util"
)

var (
	typesMap        map[string]string
	unknownTypesMsg []string

	tmpl   *template.Template
	tcTmpl *template.Template
)

type gen struct{}

func init() {
	var err error
	tmpl, err = template.New("template").Parse(codeTmpl)
	if err != nil {
		logrus.Panicf("go: failed to parse template: %s", err)
	}
	tcTmpl, err = template.New("testCaseTemplate").Parse(testTmpl)
	if err != nil {
		logrus.Panicf("go: failed to parse test case template: %s", err)
	}
	initTypes()
	g := &gen{}
	lib.Register(lib.Go, g)
}

func initTypes() {
	typesMap = make(map[string]string)
	typesMap["byte"] = "int8"
	typesMap["short"] = "int16"
	typesMap["int"] = "int"
	typesMap["long"] = "int64"
	typesMap["float"] = "float32"
	typesMap["double"] = "float64"
	typesMap["boolean"] = "bool"
	typesMap["string"] = "string"
	// Pointer: *type
	// Array: type[]
	// Both: *type[]
}

func (g *gen) Generate(in model.Input) (*model.GeneratedTest, error) {
	// Template
	unknownTypesMsg = make([]string, 0)
	gen := &model.GeneratedTest{
		Language: lib.Go,
	}

	pArgs, err := parseParams(in.Function.Params)
	if err != nil {
		return nil, err
	}
	pRets, err := parseParams(in.Function.Returns)
	if err != nil {
		return nil, err
	}

	tmpl, err := genTemplate(&in.Function, pArgs, pRets)
	if err != nil {
		return nil, err
	}
	gen.Template = tmpl
	// Fixture
	unknownTypesMsg = make([]string, 0)
	fixtures, err := genTestCases(&in, pArgs, pRets, false)
	if err != nil {
		return nil, err
	}
	gen.Fixture = fixtures
	// Test case
	unknownTypesMsg = make([]string, 0)
	tcs, err := genTestCases(&in, pArgs, pRets, true)
	if err != nil {
		return nil, err
	}
	gen.TestCase = tcs
	return gen, nil
}

func genTemplate(f *model.Function, pArgs []model.Param, pRets []model.Param) (string, error) {
	iFunc, err := parseFunction(f, pArgs, pRets)
	if err != nil {
		return "", err
	}
	b := &bytes.Buffer{}
	tmpl.Execute(b, iFunc)
	if len(unknownTypesMsg) != 0 {
		return fmt.Sprintf("// TODO: Please define or correct unknown type(s): %s\n\n",
			strings.Join(unknownTypesMsg, ", ")) + b.String(), nil
	}
	return b.String(), nil
}

func parseFunction(f *model.Function, pArgs []model.Param, pRets []model.Param) (*model.InternalFunc, error) {
	iFunc := &model.InternalFunc{
		Name: util.ToCamelCase(f.Name, true),
	}
	params := ""
	for _, p := range pArgs {
		params += fmt.Sprintf("%s %s, ", p.Name, p.Type)
	}
	iFunc.Param = strings.TrimSuffix(params, ", ")

	rets := ""
	for _, r := range pRets {
		rets += r.Type + ", "
	}
	rets = strings.TrimSuffix(rets, ", ")
	if len(f.Returns) > 1 {
		rets = fmt.Sprintf("(%s)", rets)
	}
	iFunc.Return = rets

	return iFunc, nil
}

func genTestCases(in *model.Input, pArgs []model.Param, pRets []model.Param, isTestCase bool) (string, error) {
	iTc := &model.InternalTestCase{}
	desc := in.Fixtures.Describe
	if isTestCase {
		desc = in.TestCases.Describe
	}
	if desc == "" {
		desc = util.ToCamelCase(in.Function.Name, true)
	}

	iTc.Describe = desc

	fName := util.ToCamelCase(in.Function.Name, true)
	for i, c := range in.Fixtures.Cases {
		tc, err := generateCase(c, pArgs, pRets, fName, i)
		if err != nil {
			return "", err
		}
		iTc.Cases += tc + "\n"
	}
	if isTestCase {
		for i, c := range in.TestCases.Cases {
			tc, err := generateCase(c, pArgs, pRets, fName, i+len(in.Fixtures.Cases))
			if err != nil {
				return "", err
			}
			iTc.Cases += tc + "\n"
		}
	}
	iTc.Cases = strings.TrimSuffix(iTc.Cases, "\n\n")

	b := &bytes.Buffer{}
	tcTmpl.Execute(b, iTc)
	if len(unknownTypesMsg) != 0 {
		return fmt.Sprintf("// TODO: Please define or correct unknown type(s): %s\n\n",
			strings.Join(unknownTypesMsg, ", ")) + b.String(), nil
	}
	return b.String(), nil
}

func parseParams(params []string) ([]model.Param, error) {
	ret := make([]model.Param, 0)

	for i, p := range params {
		spl := strings.Split(p, " ") // a int
		pName, pType := "", ""
		switch {
		case len(spl) == 1:
			pName = fmt.Sprintf("p%d", i)
			pType = spl[0]
		case len(spl) == 2:
			pName = spl[0]
			pType = spl[1]
		default:
			return nil, fmt.Errorf("go: invalid function parameter: %s", p)
		}

		pArgs := model.Param{
			Name: pName,
		}
		getActualType(&pArgs, pType)
		ret = append(ret, pArgs)
	}
	return ret, nil
}

func getActualType(p *model.Param, t string) {
	if strings.HasPrefix(t, "*") {
		p.IsPointer = true
		t = strings.TrimPrefix(t, "*")
	}
	if strings.HasSuffix(t, "[]") {
		p.IsArray = true
		t = strings.TrimSuffix(t, "[]")
	}

	if v, ok := typesMap[t]; ok {
		t = v
	} else {
		unknownTypesMsg = append(unknownTypesMsg, t)
	}
	if p.IsArray {
		t = "[]" + t
	}
	if p.IsPointer {
		t = "*" + t
	}
	p.Type = t
}

func generateCase(c model.Case, pArgs []model.Param, pRets []model.Param, fName string, idx int) (string, error) {
	baseTabs := "\t\t"
	ret := fmt.Sprintf("%s// Test case #%d\n", baseTabs, idx)
	if c.It == "" {
		c.It = fmt.Sprintf("Test case #%d", idx)
	}
	ret += fmt.Sprintf("%sIt(\"%s\", func() {\n", baseTabs, c.It)
	for i, a := range c.Args {
		if pArgs[i].IsArray {
			a = strings.TrimPrefix(strings.TrimSuffix(a, "]"), "[")
			ret += fmt.Sprintf("%s\tin%d%d := %s{%s}\n", baseTabs, idx, i, pArgs[i].Type, a)
		} else {
			ret += fmt.Sprintf("%s\tvar in%d%d %s = %s\n", baseTabs, idx, i, pArgs[i].Type, a)
		}
	}

	ret += baseTabs + "\t"
	for i := range pRets {
		ret += fmt.Sprintf("ret%d%d, ", idx, i)
	}
	ret = strings.TrimSuffix(ret, ", ")
	ret += fmt.Sprintf(" := %s(", fName)
	for i := range c.Args {
		if pArgs[i].IsPointer {
			ret += fmt.Sprintf("&in%d%d, ", idx, i)
			continue
		}
		ret += fmt.Sprintf("in%d%d, ", idx, i)
	}

	ret = strings.TrimSuffix(ret, ", ") + ")\n"
	for i, e := range c.Expected {
		if pRets[i].IsArray {
			e = strings.TrimPrefix(strings.TrimSuffix(e, "]"), "[")
			ret += fmt.Sprintf("%s\tex%d%d := %s{%s}\n", baseTabs, idx, i, pRets[i].Type, e)
		} else {
			ret += fmt.Sprintf("%s\tvar ex%d%d %s = %s\n", baseTabs, idx, i, pRets[i].Type, e)
		}
		ret += fmt.Sprintf("%s\tExpect(ret%d%d).To(Equal(ex%d%d))", baseTabs, idx, i, idx, i)
	}
	ret += fmt.Sprintf("\n%s})\n", baseTabs)
	return ret, nil
}

// Templates
var (
	codeTmpl = `package solution
{{.Imports}}
func {{.Name}}({{.Param}}) {{.Return}} {
  // your code here

}
`
	testTmpl = `package solution_test

import (
  . "github.com/onsi/ginkgo"
  . "github.com/onsi/gomega"
  . "codewarrior/solution"
)
{{.Imports}}
var _ = Describe("{{.Describe}}", func() {
{{.Cases}}
})
`
)
