package kotlin

import (
	"bytes"
	"fmt"
	"strings"
	"text/template"

	"github.com/sirupsen/logrus"
	"gitlab.com/fighters-solution/dynamo/lib"
	"gitlab.com/fighters-solution/dynamo/lib/model"
	"gitlab.com/fighters-solution/dynamo/lib/util"
)

var (
	typesMap        map[string]string
	unknownTypesMsg []string

	tmpl   *template.Template
	tcTmpl *template.Template
)

type gen struct{}

func init() {
	var err error
	tmpl, err = template.New("template").Parse(codeTmpl)
	if err != nil {
		logrus.Panicf("kotlin: failed to parse template: %s", err)
	}
	tcTmpl, err = template.New("testCaseTemplate").Parse(testTmpl)
	if err != nil {
		logrus.Panicf("kotlin: failed to parse test case template: %s", err)
	}
	initTypes()
	g := &gen{}
	lib.Register(lib.Kotlin, g)
}

func initTypes() {
	typesMap = make(map[string]string)
	typesMap["byte"] = "Byte"
	typesMap["short"] = "Short"
	typesMap["int"] = "Int"
	typesMap["long"] = "Long"
	typesMap["float"] = "Float"
	typesMap["double"] = "Double"
	typesMap["boolean"] = "Boolean"
	typesMap["string"] = "String"
	// Array: type[]
}

func (g *gen) Generate(in model.Input) (*model.GeneratedTest, error) {
	// Template
	unknownTypesMsg = make([]string, 0)
	gen := &model.GeneratedTest{
		Language: lib.Kotlin,
	}

	pArgs, err := parseParams(in.Function.Params)
	if err != nil {
		return nil, err
	}
	pRets, err := parseParams(in.Function.Returns)
	if err != nil {
		return nil, err
	}
	normalizeTestCases(&in, pArgs, pRets)

	tmpl, err := genTemplate(&in.Function, pArgs, pRets)
	if err != nil {
		return nil, err
	}
	gen.Template = tmpl
	// Fixture
	unknownTypesMsg = make([]string, 0)
	fixture, err := genTestCase(&in, pArgs, pRets, false)
	if err != nil {
		return nil, err
	}
	gen.Fixture = fixture
	// Test case
	unknownTypesMsg = make([]string, 0)
	tc, err := genTestCase(&in, pArgs, pRets, true)
	if err != nil {
		return nil, err
	}
	gen.TestCase = tc
	return gen, nil
}

func genTemplate(f *model.Function, pArgs []model.Param, pRets []model.Param) (string, error) {
	iFunc, err := parseFunction(f, pArgs, pRets)
	if err != nil {
		return "", err
	}
	b := &bytes.Buffer{}
	tmpl.Execute(b, iFunc)
	if len(unknownTypesMsg) != 0 {
		return fmt.Sprintf("// TODO: Please define or correct unknown type(s): %s\n\n",
			strings.Join(unknownTypesMsg, ", ")) + b.String(), nil
	}
	return b.String(), nil
}

func parseFunction(f *model.Function, pArgs []model.Param, pRets []model.Param) (*model.InternalFunc, error) {
	iFunc := &model.InternalFunc{
		Name: util.ToCamelCase(f.Name, true),
	}
	params := ""
	for _, p := range pArgs {
		if p.IsArray {
			params += fmt.Sprintf("%s: %sArray, ", p.Name, p.Type)
			continue
		}
		params += fmt.Sprintf("%s: %s, ", p.Name, p.Type)
	}
	iFunc.Param = strings.TrimSuffix(params, ", ")

	rets := ""
	if pRets[0].IsArray {
		rets += fmt.Sprintf("%sArray", pRets[0].Type)
	} else {
		rets += pRets[0].Type
	}
	iFunc.Return = rets
	return iFunc, nil
}

func genTestCase(in *model.Input, pArgs []model.Param, pRets []model.Param, isTestCase bool) (string, error) {
	iTc := &model.InternalTestCase{}
	desc := in.Fixtures.Describe
	if isTestCase {
		desc = in.TestCases.Describe
	}
	if desc == "" {
		desc = util.ToCamelCase(in.Function.Name, true)
	}
	iTc.Describe = desc

	fName := util.ToCamelCase(in.Function.Name, true)
	for i, c := range in.Fixtures.Cases {
		tc, err := generateCase(c, pArgs, pRets, fName, i)
		if err != nil {
			return "", err
		}
		iTc.Cases += tc + "\n"
	}
	if isTestCase {
		for i, c := range in.TestCases.Cases {
			tc, err := generateCase(c, pArgs, pRets, fName, i+len(in.Fixtures.Cases))
			if err != nil {
				return "", err
			}
			iTc.Cases += tc + "\n"
		}
	}
	iTc.Cases = strings.TrimSuffix(iTc.Cases, "\n\n")

	b := &bytes.Buffer{}
	tcTmpl.Execute(b, iTc)
	ret := fmt.Sprintf(b.String(), fName) // Add func name to Test%s class
	if len(unknownTypesMsg) != 0 {
		return fmt.Sprintf("// TODO: Please define or correct unknown type(s): %s\n\n",
			strings.Join(unknownTypesMsg, ", ")) + ret, nil
	}
	return ret, nil
}

func parseParams(params []string) ([]model.Param, error) {
	ret := make([]model.Param, 0)

	for i, p := range params {
		spl := strings.Split(p, " ") // a int
		pName, pType := "", ""
		switch {
		case len(spl) == 1:
			pName = fmt.Sprintf("p%d", i)
			pType = spl[0]
		case len(spl) == 2:
			pName = spl[0]
			pType = spl[1]
		default:
			return nil, fmt.Errorf("kotlin: invalid function parameter: %s", p)
		}

		pArgs := model.Param{
			Name: pName,
		}
		getActualType(&pArgs, pType)
		ret = append(ret, pArgs)
	}
	return ret, nil
}

func getActualType(p *model.Param, t string) {
	if strings.HasSuffix(t, "[]") {
		p.IsArray = true
		t = strings.TrimSuffix(t, "[]")
	}
	if v, ok := typesMap[t]; ok {
		t = v
	} else {
		unknownTypesMsg = append(unknownTypesMsg, t)
	}
	p.Type = t
}

func generateCase(c model.Case, pArgs []model.Param, pRets []model.Param, fName string, idx int) (string, error) {
	baseTabs := "\t\t"
	ret := fmt.Sprintf("%s// Test case #%d\n", baseTabs, idx)
	if c.It == "" {
		c.It = fmt.Sprintf("Test case #%d", idx)
	}
	ret += fmt.Sprintf("%s@Test\n", baseTabs)
	ret += fmt.Sprintf("%sfun testCase%d() {\n", baseTabs, idx)
	for i, a := range c.Args {
		iName := fmt.Sprintf("%d%d", idx, i)
		if pArgs[i].IsArray {
			a = strings.TrimPrefix(strings.TrimSuffix(a, "]"), "[")
			ret += fmt.Sprintf("%s\tval in%s: %sArray = %sArrayOf(%s)\n", baseTabs, iName, pArgs[i].Type, strings.ToLower(pArgs[i].Type), a)
		} else {
			ret += fmt.Sprintf("%s\tval in%s: %s = %s\n", baseTabs, iName, pArgs[i].Type, a)
		}
	}
	if pRets[0].IsArray {
		ret += fmt.Sprintf("%s\tval ret%d: %sArray = %s(", baseTabs, idx, pRets[0].Type, fName)
	} else {
		ret += fmt.Sprintf("%s\tval ret%d: %s = %s(", baseTabs, idx, pRets[0].Type, fName)
	}
	for i := range c.Args {
		ret += fmt.Sprintf("in%d%d, ", idx, i)
	}
	ret = strings.TrimSuffix(ret, ", ") + ")\n"
	if pRets[0].IsArray {
		c.Expected[0] = strings.TrimPrefix(strings.TrimSuffix(c.Expected[0], "]"), "[")
		ret += fmt.Sprintf("%s\tval ex%d: %sArray = %sArrayOf(%s)\n", baseTabs, idx, pRets[0].Type, strings.ToLower(pRets[0].Type), c.Expected[0])
		ret += fmt.Sprintf("%s\tassertTrue(ret%d contentEquals ex%d, \"%s\")\n", baseTabs, idx, idx, c.It)
	} else {
		ret += fmt.Sprintf("%s\tval ex%d: %s = %s\n", baseTabs, idx, pRets[0].Type, c.Expected[0])
		ret += fmt.Sprintf("%s\tassertEquals(ret%d, ex%d, \"%s\")\n", baseTabs, idx, idx, c.It)
	}
	ret += fmt.Sprintf("%s}\n", baseTabs)
	return ret, nil
}

func normalizeTestCases(in *model.Input, pArgs []model.Param, pRets []model.Param) {
	for i, a := range pArgs {
		if a.Type == "Long" {
			for j := range in.Fixtures.Cases {
				if a.IsArray {
					if in.Fixtures.Cases[j].Args[i] == "[]" {
						continue
					}
					in.Fixtures.Cases[j].Args[i] = strings.Replace(in.Fixtures.Cases[j].Args[i], ",", "L,", -1)
					in.Fixtures.Cases[j].Args[i] = strings.Replace(in.Fixtures.Cases[j].Args[i], "]", "L]", -1)
				} else {
					in.Fixtures.Cases[j].Args[i] += "L"
				}
			}
			for j := range in.TestCases.Cases {
				if a.IsArray {
					if in.TestCases.Cases[j].Args[i] == "[]" {
						continue
					}
					in.TestCases.Cases[j].Args[i] = strings.Replace(in.TestCases.Cases[j].Args[i], ",", "L,", -1)
					in.TestCases.Cases[j].Args[i] = strings.Replace(in.TestCases.Cases[j].Args[i], "]", "L]", -1)
				} else {
					in.TestCases.Cases[j].Args[i] += "L"
				}
			}
		} else if a.Type == "Float" {
			for j := range in.Fixtures.Cases {
				if a.IsArray {
					if in.Fixtures.Cases[j].Args[i] == "[]" {
						continue
					}
					in.Fixtures.Cases[j].Args[i] = strings.Replace(in.Fixtures.Cases[j].Args[i], ",", "F,", -1)
					in.Fixtures.Cases[j].Args[i] = strings.Replace(in.Fixtures.Cases[j].Args[i], "]", "F]", -1)
				} else {
					in.Fixtures.Cases[j].Args[i] += "F"
				}
			}
			for j := range in.TestCases.Cases {
				if a.IsArray {
					if in.TestCases.Cases[j].Args[i] == "[]" {
						continue
					}
					in.TestCases.Cases[j].Args[i] = strings.Replace(in.TestCases.Cases[j].Args[i], ",", "F,", -1)
					in.TestCases.Cases[j].Args[i] = strings.Replace(in.TestCases.Cases[j].Args[i], "]", "F]", -1)
				} else {
					in.TestCases.Cases[j].Args[i] += "F"
				}
			}
		}
	}

	for i, a := range pRets {
		if a.Type == "Long" {
			for j := range in.Fixtures.Cases {
				if a.IsArray {
					if in.Fixtures.Cases[j].Expected[i] == "[]" {
						continue
					}
					in.Fixtures.Cases[j].Expected[i] = strings.Replace(in.Fixtures.Cases[j].Expected[i], ",", "L,", -1)
					in.Fixtures.Cases[j].Expected[i] = strings.Replace(in.Fixtures.Cases[j].Expected[i], "]", "L]", -1)
				} else {
					in.Fixtures.Cases[j].Expected[i] += "L"
				}
			}
			for j := range in.TestCases.Cases {
				if a.IsArray {
					if in.TestCases.Cases[j].Expected[i] == "[]" {
						continue
					}
					in.TestCases.Cases[j].Expected[i] = strings.Replace(in.TestCases.Cases[j].Expected[i], ",", "L,", -1)
					in.TestCases.Cases[j].Expected[i] = strings.Replace(in.TestCases.Cases[j].Expected[i], "]", "L]", -1)
				} else {
					in.TestCases.Cases[j].Expected[i] += "L"
				}
			}
		} else if a.Type == "Float" {
			for j := range in.Fixtures.Cases {
				if a.IsArray {
					if in.Fixtures.Cases[j].Expected[i] == "[]" {
						continue
					}
					in.Fixtures.Cases[j].Expected[i] = strings.Replace(in.Fixtures.Cases[j].Expected[i], ",", "F,", -1)
					in.Fixtures.Cases[j].Expected[i] = strings.Replace(in.Fixtures.Cases[j].Expected[i], "]", "F]", -1)
				} else {
					in.Fixtures.Cases[j].Expected[i] += "F"
				}
			}
			for j := range in.TestCases.Cases {
				if a.IsArray {
					if in.TestCases.Cases[j].Expected[i] == "[]" {
						continue
					}
					in.TestCases.Cases[j].Expected[i] = strings.Replace(in.TestCases.Cases[j].Expected[i], ",", "F,", -1)
					in.TestCases.Cases[j].Expected[i] = strings.Replace(in.TestCases.Cases[j].Expected[i], "]", "F]", -1)
				} else {
					in.TestCases.Cases[j].Expected[i] += "F"
				}
			}
		}
	}
}

// Templates
var (
	codeTmpl = `package solution

fun {{.Name}}({{.Param}}): {{.Return}} {
  // your code here

}
`
	testTmpl = `package solution
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import org.junit.Test
{{.Imports}}
class Test%s {
{{.Cases}}
}
`
)
