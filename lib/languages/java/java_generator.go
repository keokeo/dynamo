package java

import (
	"bytes"
	"fmt"
	"strings"
	"text/template"

	"github.com/sirupsen/logrus"
	"gitlab.com/fighters-solution/dynamo/lib"
	"gitlab.com/fighters-solution/dynamo/lib/model"
	"gitlab.com/fighters-solution/dynamo/lib/util"
)

var (
	typesMap        map[string]string
	unknownTypesMsg []string

	tmpl   *template.Template
	tcTmpl *template.Template

	isContainList bool
)

type gen struct{}

func init() {
	var err error
	tmpl, err = template.New("template").Parse(codeTmpl)
	if err != nil {
		logrus.Panicf("java: failed to parse template: %s", err)
	}
	tcTmpl, err = template.New("testCaseTemplate").Parse(testTmpl)
	if err != nil {
		logrus.Panicf("java: failed to parse test case template: %s", err)
	}
	initTypes()
	g := &gen{}
	lib.Register(lib.Java, g)
}

func initTypes() {
	typesMap = make(map[string]string)
	typesMap["byte"] = "Byte"
	typesMap["short"] = "Short"
	typesMap["int"] = "Integer"
	typesMap["long"] = "Long"
	typesMap["float"] = "Float"
	typesMap["double"] = "Double"
	typesMap["boolean"] = "Boolean"
	typesMap["string"] = "String"
	// Array: type[]
}

func (g *gen) Generate(in model.Input) (*model.GeneratedTest, error) {
	// Template
	unknownTypesMsg = make([]string, 0)
	gen := &model.GeneratedTest{
		Language: lib.Java,
	}
	isContainList = false

	pArgs, err := parseParams(in.Function.Params)
	if err != nil {
		return nil, err
	}
	pRets, err := parseParams(in.Function.Returns)
	if err != nil {
		return nil, err
	}
	normalizeTestCases(&in, pArgs, pRets)

	tmpl, err := genTemplate(&in.Function, pArgs, pRets)
	if err != nil {
		return nil, err
	}
	gen.Template = tmpl
	// Fixture
	unknownTypesMsg = make([]string, 0)
	fixture, err := genTestCase(&in, pArgs, pRets, false)
	if err != nil {
		return nil, err
	}
	gen.Fixture = fixture
	// Test case
	unknownTypesMsg = make([]string, 0)
	tc, err := genTestCase(&in, pArgs, pRets, true)
	if err != nil {
		return nil, err
	}
	gen.TestCase = tc
	return gen, nil
}

func genTemplate(f *model.Function, pArgs []model.Param, pRets []model.Param) (string, error) {
	iFunc, err := parseFunction(f, pArgs, pRets)
	if err != nil {
		return "", err
	}
	if isContainList {
		iFunc.Imports = "import java.util.List;\n\n"
	}
	b := &bytes.Buffer{}
	tmpl.Execute(b, iFunc)
	if len(unknownTypesMsg) != 0 {
		return fmt.Sprintf("// TODO: Please define or correct unknown type(s): %s\n\n",
			strings.Join(unknownTypesMsg, ", ")) + b.String(), nil
	}
	return b.String(), nil
}

func parseFunction(f *model.Function, pArgs []model.Param, pRets []model.Param) (*model.InternalFunc, error) {
	iFunc := &model.InternalFunc{
		Name: util.ToCamelCase(f.Name, false),
	}
	params := ""
	for _, p := range pArgs {
		if p.IsArray {
			params += fmt.Sprintf("List<%s> %s, ", p.Type, p.Name)
			continue
		}
		params += fmt.Sprintf("%s %s, ", p.Type, p.Name)
	}
	iFunc.Param = strings.TrimSuffix(params, ", ")

	rets := ""
	if pRets[0].IsArray {
		rets += fmt.Sprintf("List<%s>", pRets[0].Type)
	} else {
		rets += pRets[0].Type
	}
	iFunc.Return = rets
	return iFunc, nil
}

func genTestCase(in *model.Input, pArgs []model.Param, pRets []model.Param, isTestCase bool) (string, error) {
	iTc := &model.InternalTestCase{}
	desc := in.Fixtures.Describe
	if isTestCase {
		desc = in.TestCases.Describe
	}
	if desc == "" {
		desc = util.ToCamelCase(in.Function.Name, true)
	}
	iTc.Describe = desc

	fName := util.ToCamelCase(in.Function.Name, false)
	for i, c := range in.Fixtures.Cases {
		tc, err := generateCase(c, pArgs, pRets, fName, i)
		if err != nil {
			return "", err
		}
		iTc.Cases += tc + "\n"
	}
	if isTestCase {
		for i, c := range in.TestCases.Cases {
			tc, err := generateCase(c, pArgs, pRets, fName, i+len(in.Fixtures.Cases))
			if err != nil {
				return "", err
			}
			iTc.Cases += tc + "\n"
		}
	}
	iTc.Cases = strings.TrimSuffix(iTc.Cases, "\n\n")

	if isContainList {
		iTc.Imports = "import java.util.List;\nimport java.util.ArrayList;\nimport java.util.Arrays;\n"
	}
	b := &bytes.Buffer{}
	tcTmpl.Execute(b, iTc)
	ret := fmt.Sprintf(b.String(), strings.ToUpper(string(fName[0]))+string(fName[1:])) // Add func name as Test%s class
	if len(unknownTypesMsg) != 0 {
		return fmt.Sprintf("// TODO: Please define or correct unknown type(s): %s\n\n",
			strings.Join(unknownTypesMsg, ", ")) + ret, nil
	}
	return ret, nil
}

func parseParams(params []string) ([]model.Param, error) {
	ret := make([]model.Param, 0)

	for i, p := range params {
		spl := strings.Split(p, " ") // a int
		pName, pType := "", ""
		switch {
		case len(spl) == 1:
			pName = fmt.Sprintf("p%d", i)
			pType = spl[0]
		case len(spl) == 2:
			pName = spl[0]
			pType = spl[1]
		default:
			return nil, fmt.Errorf("java: invalid function parameter: %s", p)
		}

		pArgs := model.Param{
			Name: pName,
		}
		getActualType(&pArgs, pType)
		ret = append(ret, pArgs)
	}
	return ret, nil
}

func getActualType(p *model.Param, t string) {
	if strings.HasSuffix(t, "[]") {
		isContainList = true
		p.IsArray = true
		t = strings.TrimSuffix(t, "[]")
	}

	if v, ok := typesMap[t]; ok {
		t = v
	} else {
		unknownTypesMsg = append(unknownTypesMsg, t)
	}
	p.Type = t
}

func generateCase(c model.Case, pArgs []model.Param, pRets []model.Param, fName string, idx int) (string, error) {
	baseTabs := "\t"
	ret := fmt.Sprintf("%s// Test case #%d\n", baseTabs, idx)
	if c.It == "" {
		c.It = fmt.Sprintf("Test case #%d", idx)
	}

	ret += fmt.Sprintf("%s@Test\n", baseTabs)
	ret += fmt.Sprintf("%spublic void testCase%d() {\n", baseTabs, idx)
	ret += fmt.Sprintf("%s\tSolution sol = new Solution();\n", baseTabs)
	for i, a := range c.Args {
		if pArgs[i].IsArray {
			iName := fmt.Sprintf("%d%d", idx, i)
			a = strings.TrimPrefix(strings.TrimSuffix(a, "]"), "[")
			ret += fmt.Sprintf("%s\t%s[] tmp%s_1 = new %s[]{%s};\n", baseTabs, pArgs[i].Type,
				iName, pArgs[i].Type, a)
			ret += fmt.Sprintf("%s\tList<%s> in%s = new ArrayList<%s>(Arrays.asList(tmp%s_1));\n", baseTabs,
				pArgs[i].Type, iName, pArgs[i].Type, iName)
		} else {
			ret += fmt.Sprintf("%s\t%s in%d%d = new %s(%s);\n", baseTabs, pArgs[i].Type, idx, i, pArgs[i].Type, a)
		}
	}

	if pRets[0].IsArray {
		c.Expected[0] = strings.TrimPrefix(strings.TrimSuffix(c.Expected[0], "]"), "[")
		ret += fmt.Sprintf("%s\t%s[] tmp_ex_%d = new %s[]{%s};\n", baseTabs, pRets[0].Type,
			idx, pRets[0].Type, c.Expected[0])
		ret += fmt.Sprintf("%s\tList<%s> ex%d = new ArrayList<%s>(Arrays.asList(tmp_ex_%d));\n", baseTabs,
			pRets[0].Type, idx, pRets[0].Type, idx)
	} else {
		ret += fmt.Sprintf("%s\t%s ex%d = new %s(%s);\n", baseTabs, pRets[0].Type, idx, pRets[0].Type, c.Expected[0])
	}

	ret += fmt.Sprintf("\n%s\tlong startTime = System.nanoTime();\n", baseTabs)
	if pRets[0].IsArray {
		ret += fmt.Sprintf("%s\tList<%s> ret%d = sol.%s(", baseTabs, pRets[0].Type, idx, fName)
	} else {
		ret += fmt.Sprintf("%s\t%s ret%d = sol.%s(", baseTabs, pRets[0].Type, idx, fName)
	}
	for i := range c.Args {
		ret += fmt.Sprintf("in%d%d, ", idx, i)
	}
	ret = strings.TrimSuffix(ret, ", ") + ");\n"
	ret += fmt.Sprintf("%s\tassertEquals(\"%s\", ret%d, ex%d);\n", baseTabs, c.It, idx, idx)
	ret += fmt.Sprintf("%s\tSystem.out.println(\"<EXECTIME::>\" + ((System.nanoTime() - startTime) / 1e6));\n", baseTabs)
	ret += fmt.Sprintf("%s}\n", baseTabs)
	return ret, nil
}

func normalizeTestCases(in *model.Input, pArgs []model.Param, pRets []model.Param) {
	for i, a := range pArgs {
		if a.Type == "Long" {
			for j := range in.Fixtures.Cases {
				if a.IsArray {
					if in.Fixtures.Cases[j].Args[i] == "[]" {
						continue
					}
					in.Fixtures.Cases[j].Args[i] = strings.Replace(in.Fixtures.Cases[j].Args[i], ",", "L,", -1)
					in.Fixtures.Cases[j].Args[i] = strings.Replace(in.Fixtures.Cases[j].Args[i], "]", "L]", -1)
				} else {
					in.Fixtures.Cases[j].Args[i] += "L"
				}
			}
			for j := range in.TestCases.Cases {
				if a.IsArray {
					if in.TestCases.Cases[j].Args[i] == "[]" {
						continue
					}
					in.TestCases.Cases[j].Args[i] = strings.Replace(in.TestCases.Cases[j].Args[i], ",", "L,", -1)
					in.TestCases.Cases[j].Args[i] = strings.Replace(in.TestCases.Cases[j].Args[i], "]", "L]", -1)
				} else {
					in.TestCases.Cases[j].Args[i] += "L"
				}
			}
		} else if a.Type == "Float" {
			for j := range in.Fixtures.Cases {
				if a.IsArray {
					if in.Fixtures.Cases[j].Args[i] == "[]" {
						continue
					}
					in.Fixtures.Cases[j].Args[i] = strings.Replace(in.Fixtures.Cases[j].Args[i], ",", "F,", -1)
					in.Fixtures.Cases[j].Args[i] = strings.Replace(in.Fixtures.Cases[j].Args[i], "]", "F]", -1)
				} else {
					in.Fixtures.Cases[j].Args[i] += "F"
				}
			}
			for j := range in.TestCases.Cases {
				if a.IsArray {
					if in.TestCases.Cases[j].Args[i] == "[]" {
						continue
					}
					in.TestCases.Cases[j].Args[i] = strings.Replace(in.TestCases.Cases[j].Args[i], ",", "F,", -1)
					in.TestCases.Cases[j].Args[i] = strings.Replace(in.TestCases.Cases[j].Args[i], "]", "F]", -1)
				} else {
					in.TestCases.Cases[j].Args[i] += "F"
				}
			}
		}
	}

	for i, a := range pRets {
		if a.Type == "Long" {
			for j := range in.Fixtures.Cases {
				if a.IsArray {
					if in.Fixtures.Cases[j].Expected[i] == "[]" {
						continue
					}
					in.Fixtures.Cases[j].Expected[i] = strings.Replace(in.Fixtures.Cases[j].Expected[i], ",", "L,", -1)
					in.Fixtures.Cases[j].Expected[i] = strings.Replace(in.Fixtures.Cases[j].Expected[i], "]", "L]", -1)
				} else {
					in.Fixtures.Cases[j].Expected[i] += "L"
				}
			}
			for j := range in.TestCases.Cases {
				if a.IsArray {
					if in.TestCases.Cases[j].Expected[i] == "[]" {
						continue
					}
					in.TestCases.Cases[j].Expected[i] = strings.Replace(in.TestCases.Cases[j].Expected[i], ",", "L,", -1)
					in.TestCases.Cases[j].Expected[i] = strings.Replace(in.TestCases.Cases[j].Expected[i], "]", "L]", -1)
				} else {
					in.TestCases.Cases[j].Expected[i] += "L"
				}
			}
		} else if a.Type == "Float" {
			for j := range in.Fixtures.Cases {
				if a.IsArray {
					if in.Fixtures.Cases[j].Expected[i] == "[]" {
						continue
					}
					in.Fixtures.Cases[j].Expected[i] = strings.Replace(in.Fixtures.Cases[j].Expected[i], ",", "F,", -1)
					in.Fixtures.Cases[j].Expected[i] = strings.Replace(in.Fixtures.Cases[j].Expected[i], "]", "F]", -1)
				} else {
					in.Fixtures.Cases[j].Expected[i] += "F"
				}
			}
			for j := range in.TestCases.Cases {
				if a.IsArray {
					if in.TestCases.Cases[j].Expected[i] == "[]" {
						continue
					}
					in.TestCases.Cases[j].Expected[i] = strings.Replace(in.TestCases.Cases[j].Expected[i], ",", "F,", -1)
					in.TestCases.Cases[j].Expected[i] = strings.Replace(in.TestCases.Cases[j].Expected[i], "]", "F]", -1)
				} else {
					in.TestCases.Cases[j].Expected[i] += "F"
				}
			}
		}
	}
}

// Templates
var (
	codeTmpl = `{{.Imports}}public class Solution {
  public {{.Return}} {{.Name}}({{.Param}}) {
    // your code here

  }
}
`
	testTmpl = `import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.runners.JUnit4;
{{.Imports}}
public class Test%s {
{{.Cases}}
}
`
)
