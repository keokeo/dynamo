FROM alpine:3.8
MAINTAINER "Rye Team, Quy Le <lnquy@tma.com.vn>"
LABEL project="codefighter" service="dynamo"

WORKDIR /home/
COPY dynamo.bin .
RUN chmod +x dynamo.bin

EXPOSE 14005
CMD ["./dynamo.bin"]
